package com.demo.tictactoe;

import android.content.Context;
import android.widget.TextView;

public class Game {
	
	static final int N_PLAYERS = 2;
	static int nMoves = 0;				// Numbers of moves 
	
	/* Game constructor */
	public Game(TextView[] UIWidgets, Context context)
	{
		this.context = context;
		
		player[0] = new ComputerPlayer(Square.CIRCLE_PIECE, context);
		player[1] = new HumanPlayer(Square.CROSS_PIECE, context);
		
		board = new Board(UIWidgets);
		
		player[0].useBoard(board);
		player[1].useBoard(board);		
	}
	
	public HumanPlayer getHumanPlayer()
	{
		return (HumanPlayer) this.player[1];
	}
	
	public ComputerPlayer getComputerPlayer()
	{
		return (ComputerPlayer) this.player[0];
	}
	
	public Board getBoard()
	{
		return board;
	}
	
	/* the players */
	private Player[] player = new Player[N_PLAYERS];
	
	/* the board */
	private Board board;
	
	/* the application context */
	Context context;
}
