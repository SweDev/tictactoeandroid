package com.demo.tictactoe;

import java.util.Random;

import android.view.View;
import android.widget.TextView;


/* -- class for a 3 by 3 tic-tac-toe board --
the board is represented by an array, which
for the case of a 3 by 3 board represents the
squares using the following numbering:
-------
|0|1|2|
-------
|3|4|5|
-------
|6|7|8|
-------
*/
class Board {
	
	/* the number of squares */
	public static final int N_SQUARES = TicTacToe.BOARD_ROW_MAX * TicTacToe.ROW_SQUARES_MAX;
	
    /* Board constructor */
	public Board(TextView[] widgets)
	{	
		int i = 0;
		
		while (i < N_SQUARES)
		{
			square[i] = new Square(widgets[i], Square.NO_PIECE, true);
			i++;
		}
	}
	
	/* is_free: checks if the square is free */
	public int getIndex(View v)
	{
		int index = 0;
		
		if(v.getId() == R.id.square_1 )
			index = 0;
		else if(v.getId() == R.id.square_2 )
			index = 1;
		else if(v.getId() == R.id.square_3 )
			index = 2;
		else if(v.getId() == R.id.square_4 )
			index = 3;
		else if(v.getId() == R.id.square_5 )
			index = 4;
		else if(v.getId() == R.id.square_6 )
			index = 5;
		else if(v.getId() == R.id.square_7 )
			index = 6;
		else if(v.getId() == R.id.square_8 )
			index = 7;
		else if(v.getId() == R.id.square_9 )
			index = 8;
		
		return index;
	}
	
	/* display: displays a board */
	public void display()
	{
		//TODO: Implement display fucntion
	}
	
	/* board_set_piece_random: places a piece of type piece_type at a
	randomly chosen position, except at the position no_set_index,
	a parameter which is used to prevent the computer from placing
	the piece at the same square as it was taken from */
	public void setPieceRandom(int pieceType, int noSetIndex)
	{
		/* maximum number of iterations */
		final int MAX_N_ITER = 100;

		/* actual number of iterations */
		int nIter;

		/* flag to indicate that a free place is found */
		boolean found;

		/* the square index where the piece shall be placed */
		int squareIndex;

		/* no iterations done */
		nIter = 0;

		/* the square is not found */
		found = false;

		/* as long as a square is not found and not too many iterations
		are performed */
		while (!found && nIter < MAX_N_ITER)
		{
			
			/* one more iteration */
			nIter++;
			
			/* select a random square, i.e. select a number between 0 and
			N_SQUARES-1 */
			squareIndex = generator.nextInt( N_SQUARES );

			/* check if the index is allowed and the square is free,
			and if this is the case, the piece is placed on the square */
			found = squareIndex != noSetIndex && square[squareIndex].isFree();
			if (found)
			{
				
				square[squareIndex].setPiece(pieceType);
				
			}
		}
		if (!found)
		{
			//TODO: Use alert dialog
			//cout << "ERROR: could not place piece" << endl;
			System.out.print("ERROR: could not place piece");

		}
	}
	
	/* is_free: checks if the square is free */
	public boolean isFree(/* input argument that indicate a specific square on the board */
						int squareIndex)
	{
		
		return square[squareIndex].isFree() ;

	}
	
	/* board_has_win_pattern: check if the human or computer has won */
	public boolean hasWinPattern(/* input argument that indicate a surtain type of piece on a square */
								 int pieceType) 
	{
		
		/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
		when 3 the same pieces are placed horozontally, vertically or diogonally */
		int[] magicSquares = {4,3,8,9,5,1,2,7,6};
		
		/* win_sum hold a number determined by magic_squares[i] and the position where
		the pieces are placed at the board,
		n_piece_type hold a number that is determined by the amount of pieces placed on 
		the board of a surtain type */
		int winSum = 0, i = 0, nPieceType = 0;
	    
		/* for statement checks if a square holds a specific piece type and if it does 
		the variabel win_sum stores a number that must be 15 for that particular piece 
		type to win the game */
		while(i < N_SQUARES)
		{			
			if(square[i].hasPiece(pieceType) == true)
			{
		    
				winSum += magicSquares[i];
				nPieceType += 1 ;

			} /* end if statment*/
			i++;
		} /* end for loop */

		/* if statement checks if a particular piece type has won. The piece type need to have the win_sum to hold the value,
		15 and there has to be exactly 3 pieces of a curtain piece type on the board.
		If a surtain piece type has a win pattern of the board this function board_has_win_pattern() returns 1 
		else it returns 0 */
	    if( ( winSum == 15 ) && ( nPieceType == 3 ) )
	      return true ;
		else
		  return false ;
	}
	
	/* has_piece: returns true if a piece of type piece_type is placed on the square, returns false otherwise */
	public boolean hasPiece(/* input argument that indicate a surtain type of piece on a square */
					  		 int pieceType, 
					  		 /* input argument that indicate a specific square on the board */
					  		 int squareIndex)
	{
		return square[squareIndex].hasPiece(pieceType);
	}
	
	/* set_piece: places a piece of type piece_type on the square */
	public void setPiece(int pieceType, int squareIndex)
	{
		square[squareIndex].setPiece(pieceType);
	}

	/* square_remove_piece: removes a piece given by piece_type from a given square */
	public void removePiece(/* input argument that indicate a specific square on the board */
							int squareIndex)
	{
		/* Here we delete a piece from a square */
		square[squareIndex].removePiece();	
	}
	
	/* is_blocking_piece: Check if a surtain piece type at a given square is a piece that
	is placed as a blocking piece between two opposing pieces that 	
	keeps the opposing piece from creating a win pattern */
	public boolean isBlockingPiece(/* input argument that indicate a specific square on the board */
								  int squareIndex, 
								  /* input argument that indicate a surtain type of piece on a square */
								  int pieceType)
	{

		/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
		when 3 the same pieces are placed horozontally, vertically or diogonally */
		int[] magicSquares = {4,3,8,9,5,1,2,7,6}; 
		
		/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece on the board */
		int[] circlePieceIndexArray = {0,0,0}; 
		
		/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece on the board */
		int[] crossPieceIndexArray = {0,0,0};
		
		/* cross_win_sum and circle_win_sum hold a number determined by magic_squares[i] and the position where
		the pieces are placed at the board */
		int crossWinSum = 0, circleWinSum = 0;
			
	    /* n_circle_piece_type and n_cross_piece_type both hold a number that is determined by the amount of pieces 
		placed on the board of a surtain type */
		int nCirclePieceType = 0, nCrossPieceType = 0 ;

		/* Indexes used for iterating array values */
		int x = 0, y = 0, i = 0; 

		/* for statement checks if a square holds a specific piece type and if it does 
		we store the exact location of that piece */
		while(i < N_SQUARES)
		{			
			if(square[i].hasPiece(pieceType) == true)
			{	
				
				circlePieceIndexArray[x] = i ;
				x++ ;
				
			} /* end if statement*/
			
			if(square[i].hasPiece(pieceType) == true)
			{
		    
				crossPieceIndexArray[y] = i ;
				y++ ;

			} /* end if statement*/
			i++;	
		} /* end for statement*/

		/* if statements checks if a square at the given location, squareIndex, is placed as a blocking piece and if it is
		   the board_is_blocking_piece() function returns 1 */
		if( ( ( magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[1]] + magicSquares[squareIndex] ) == 15 ) )
		{
			
			return true ;
			
		} /* end if statement */
		else if( ( ( magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[1]] + magicSquares[squareIndex] ) == 15 ) )
		{

			return true ;
			   
		} /* end else if statement */
		else if( ( ( magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[1]] + magicSquares[squareIndex] ) == 15 ) )
		{
			

			return true ;
			   
		} /* end else if statement */
		else if( ( ( magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[2]] + magicSquares[squareIndex] ) == 15 ) )
		{

			return true ;
			   
		} /* end else if statement */
		else if( ( ( magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[2]] + magicSquares[squareIndex] ) == 15 ) )
		{

			return true ;
			   
		} /* end else if statement */
		else if( ( ( magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[2]] + magicSquares[squareIndex] ) == 15 ) )
		{

			return true ;
			   
		} /* end else if statement */
		else if( ( ( magicSquares[crossPieceIndexArray[1]] + magicSquares[crossPieceIndexArray[2]] + magicSquares[squareIndex] ) == 15 ) )
		{

			return true ;
			   
		} /* end else if statement */
		else if( ( ( magicSquares[crossPieceIndexArray[1]] + magicSquares[crossPieceIndexArray[2]] + magicSquares[squareIndex] ) == 15 ) )
		{

			return true ;
			   
		} /* end else if statement */
		else if( ( ( magicSquares[crossPieceIndexArray[1]] + magicSquares[crossPieceIndexArray[2]] + magicSquares[squareIndex] ) == 15 ) )
		{

			return true ;
			   
		} /* end else if statement */
		else
		{

			return false ;

		} /* end else statement */

	}
	
	/* board_delete_random_circle_piece: delete a circle piece from a randomly chosen square */
	public int deleteRandomCirclePiece(/* input argument that indicate a surtain type of piece on a square */
									   int pieceType) 
	{

	  /* maximum number of iterations */
	  final int MAX_N_ITER = 100 ;
	  
	  /* actual number of iterations */
	  int nIter ;

	  /* flag to indicate that a free place is found */
	  boolean found ;
	  
	  /* the square index where the piece shall be removed from */
	  int squareIndex = 0 ;
	  
	  /* no iterations done */
	  nIter = 0 ;
	  
	  /* the square is not found */
	  found = false ;
	  
	  /* as long as a square is not found and not too many iterations
	  are performed */
	  while (!found && nIter < MAX_N_ITER)
	  {
	    
		/* one more iteration */
	    nIter++ ;
	    
		/* select a random square, i.e. select a number between 0 and
	    N_SQUARES-1 */
	    squareIndex = generator.nextInt( N_SQUARES );
	    
		/* check if the index is allowed and the square is a CIRCLE_PIECE,
	    and if this is the case, the piece is moved from the square */
	    found = (square[squareIndex].hasPiece(pieceType) == true) && ((isBlockingPiece(squareIndex, pieceType)) == false) ;
					
		//(square_index != no_set_index) && (square_is_free(&board->square[square_index]))
	    
		if (found)
		{
	      
	      /* Here we delete the CIRCLE_PIECE at the given position a, 
	      given by the human player */
		 
		  square[squareIndex].removePiece() ;
		  
		} /* end if-statement */
	  
	  } /* end while-statement */
	  
	  if (!found)
	  {
		//TODO: Use alert dialog instead
	    //printf("ERROR: could not move piece\n");
		System.out.println("ERROR: could not move piece\n");
	  
	  } /* end if-statement */


	  return squareIndex;

	}


	private Square[] square = new Square[N_SQUARES];
	
	private Random generator = new Random();
}
