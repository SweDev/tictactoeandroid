package com.demo.tictactoe;

import android.content.Context;

class ComputerPlayer extends Player {
	
	/* Computer_Player constructor */
	public ComputerPlayer(/* input arguments related to the type of piece stored on the square */
						  int pieceType,
						  Context context)
	{
		super(pieceType);
		this.context = context;
	}

	/* make_move: make a move for the computer player */
	public void makeMove(/* input arguments related to the amount of times the board_play_game runs */
						 int nMoves)
	{

		/* circle_piece_made_win_move will indicate if the computer made a stratigic move to win the game
		circle_piece_made_blocking_move will indicate if the computer made a stratigic move to block the 
		human from winning */
		boolean circlePieceMadeWinMove = false, circlePieceMadeBlockingMove = false ; 
		
		/* no_set_index is the square index where a piece can not be placed at */
		int noSetIndex = 0 ;
	  
		/*circle_piece_make_stratigic_move(board,n_moves) ;*/
		
		/* The computer makes a winning move */
		circlePieceMadeWinMove = circlePieceMakeWinMove(nMoves) ;
		
		/* If the computer has not made a win move then it does a blocking move */
		if(circlePieceMadeWinMove == false)
		{
			
			/* The computer did not make a win move so it does a blocking move instead */
			circlePieceMadeBlockingMove = circlePieceMakeBlockingMove(nMoves) ;

			/* If the computer does not do a win move or blocking move it makes a random move */
			if(circlePieceMadeBlockingMove == false && circlePieceMadeWinMove == false)
			{
				
				/* The computer makes a random move */
				circlePieceMakeNonStratigicMove(nMoves) ;
				
			} /* end if-statement */

		} /* end if-statement */

	}
	
	/* circle_piece_make_non_stratigic_move: the computer does not follow any stratagy and makes a random move */
	private void circlePieceMakeNonStratigicMove(/* input arguments related to the amount of times the board_play_game runs */
												int nMoves) 
	{

		/* A variabel that holds the index/location where a piece will not be placed */
		int noSetIndex = 0 ; 
		
		/* check if the computer have made less or exactly 3 moves and if so then do something */
		if(nMoves <= 3)
		{
			
			/* place a CIRCLE_PIECE at a random place at the board that is not the location of no_set_index */
			board.setPieceRandom(Square.CIRCLE_PIECE, noSetIndex) ;
		
		} /* end if-statement */

		/* check if the computer have made more then 3 moves and if so then do something */
		if(nMoves > 3)
		{
			/* remove a CIRCLE_PIECE at a random place at the board */
			noSetIndex = board.deleteRandomCirclePiece(Square.CIRCLE_PIECE);
			
			/* place a CIRCLE_PIECE at at a random place at the board */
			board.setPieceRandom(Square.CIRCLE_PIECE, noSetIndex);
			
		} /* end if-statement*/

	}
	
	/* circle_piece_make_win_move: the computer makes a winning move */
	private boolean circlePieceMakeWinMove(/* input arguments related to the amount of times the board_play_game runs */
										   int nMoves)
	{

		/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
		when 3 the same pieces are placed horozontally, vertically or diogonally */
		int[] magicSquares = {4,3,8,9,5,1,2,7,6} ; 
		
		/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece */
		int[] circlePieceIndexArray = {0,0,0} ; 
		
		/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece */
		int[] crossPieceIndexArray = {0,0,0} ;
		
		/* Variabels that hold the sum of the magic square values */
		int crossWinSum = 0, circleWinSum = 0 ;
			
	    /* Variabels that hold the the amount of pieces that exists on the game board */
		int nCirclePieceType = 0, nCrossPieceType = 0 ; 
		
		/* A variabel that holds the index/location where a piece will not be placed */
		int noSetIndex = 0 ; 
			 	
		/* A variabel used to keep track if a square has recently been placed on the game board */
		boolean squarePieceSet = false ; 
		
	    /* Indexes used for iterating array values */
		int x = 0, y = 0, i = 0; 
		
		/* check if the computer have made less or exactly 3 moves and if so then do something */
		if(nMoves <= 3)
		{
			/* Checks what kind of piece type that exists on the game board, when a circle piece is found
			add some value to the variable circle_win_sum and keep track on how many
			circle pieces we have on the board */
			while(i < Board.N_SQUARES)
			{	
				/* If we hava found a circle piece on the game board do something */
				if(board.hasPiece(Square.CIRCLE_PIECE,i) == true)
				{  	
					
					circleWinSum += magicSquares[i] ;
					nCirclePieceType += 1 ;

				} /* end if-statement */
				
				i++;
			} /* end while-statement */	
	        
			i = 0;
			/* The for-statement checks how if the circle pieces are placad in a specific way that can give a win pattern on the game board */
			while(i < Board.N_SQUARES)
			{
	    
				/*printf("\n21\n") ;*/
				if( (magicSquares[i] == (15-circleWinSum)) && ((board.isFree(i)) == true) && (squarePieceSet == false) && (nCirclePieceType == 2) )
				{
			       
					/* Place a circle piece on the board to create a win pattern */
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true; 

				} /* end if statement */
				i++;
			}  /* end while-statement */

		} /* end if-statement */


		/* check if the computer have made more then 3 moves and if so then do something */
		if(nMoves > 3)
		{
			i = 0;
			/* For statement checks what kind of piece type that exists on the game board, when a circle piece is found
			it's index/location gets stored in the circle_piece_index_array[i] and keep track on how many cross 
			pieces we have on the board */
			while(i < Board.N_SQUARES)
			{
				
				/* If we hava found a circle piece on the game board do something */
				if(board.hasPiece(Square.CIRCLE_PIECE,i) == true)
				{	
				  	
					circlePieceIndexArray[x] = i ;
					nCirclePieceType += 1 ;
					x++ ;

				} /* end if-statement*/
				i++;
			} /* end for-statement */
			
			i = 0 ;
			/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
			while(i < Board.N_SQUARES)
			{

				if( (magicSquares[i] == (15-(magicSquares[circlePieceIndexArray[0]] + magicSquares[circlePieceIndexArray[1]]))) && ((board.isFree(i) == true) ) && (nCirclePieceType == 3) && (squarePieceSet == false) )
				{
				   
					/* We remove a CIRCLE_PIECE and place it where it will create a win pattern */
				   
					board.removePiece(circlePieceIndexArray[2]) ;
					   		   
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true ;
			   
			   } /* end if-statement */
			   else if( (magicSquares[i] == (15-(magicSquares[circlePieceIndexArray[0]] + magicSquares[circlePieceIndexArray[2]]))) && ((board.isFree(i)) == true ) && (nCirclePieceType == 3) && (squarePieceSet == false) )
			   {
				   
				   /* We remove a CIRCLE_PIECE and place it where it will create a win pattern */

				   board.removePiece(circlePieceIndexArray[1]) ;
				   
				   board.setPiece(Square.CIRCLE_PIECE,i) ;
			       squarePieceSet = true ;
			   
			   } /* end else if-statement */
			   else if( (magicSquares[i] == (15-(magicSquares[circlePieceIndexArray[1]] + magicSquares[circlePieceIndexArray[2]]))) && ((board.isFree(i)) == true ) && (nCirclePieceType == 3) && (squarePieceSet == false) )
			   {
				   
				   /* We remove a CIRCLE_PIECE and place it where it will create a win pattern */
				   
				   board.removePiece(circlePieceIndexArray[0]) ;
				   
				   board.setPiece(Square.CIRCLE_PIECE,i) ;
			       squarePieceSet = true ;

			   } /* end else if-statement */
			
			i++;
			} /* end for-statement*/

		} /* end if-statement*/


		return squarePieceSet ;

	}
	
	/* circle_piece_make_block_move: the computer makes a move to block the human from winning */
	private boolean circlePieceMakeBlockingMove(/* input arguments related to the amount of times the board_play_game runs */
												int nMoves) 
	{

		/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
		when 3 the same pieces are placed horozontally, vertically or diogonally */
		int[] magicSquares = {4,3,8,9,5,1,2,7,6} ; 
		
		/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece */
		int[] circlePieceIndexArray = {0,0,0} ; 
		
		/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece */
		int[] crossPieceIndexArray = {0,0,0} ;
		
		/* Variabels that hold the sum of the magic square values */
		int crossWinSum = 0, circleWinSum = 0 ;
			
	    /* Variabels that hold the the amount of pieces that exists on the game board */
		int nCirclePieceType = 0, nCrossPieceType = 0 ; 
		
		/* A variabel that holds the index/location where a piece will not be placed */
		int noSetIndex = 0 ; 
			 	
		/* A variabel used to keep track if a square has recently been placed on the game board */
		boolean squarePieceSet = false ; 
		
	    /* Indexes used for iterating array values */
		int x = 0, y = 0, i = 0 ; 

		/* check if the computer have made less or exactly 3 moves and if so then do something */
		if(nMoves <= 3)
		{

			/* For statement checks what kind of piece type that exists on the game board, when a cross piece is found
			it's index/location gets stored in the cross_piece_index_array[i], add some value to the variable cross_win_sum 
			and keep track on how many cross pieces we have on the board */
			while(i < Board.N_SQUARES)
			{
			
				/* If we have found a cross piece on the game board do something */
				if(board.hasPiece(Square.CROSS_PIECE,i) == true)
				{
					
					crossWinSum += magicSquares[i] ;
					nCrossPieceType += 1 ;
		    
					crossPieceIndexArray[y] = i ;
					y++ ;

				} /* end if-statement */
				i++;
			} /* end for-statement */

			i = 0 ;
			/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
			while(i < Board.N_SQUARES)
			{
		
				if( (magicSquares[i] == (15-(magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[1]]))) && ((board.isFree(i)) == true) && (nCrossPieceType == 3) && (squarePieceSet == false))
				{
				
					/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
					from creating a win pattern */
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true ;

				} /* end if-statement */
				else if( (magicSquares[i] == (15-(magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[2]]))) && ((board.isFree(i)) == true ) && (nCrossPieceType == 3) && (squarePieceSet == false) )
				{
				
					/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
					from creating a win pattern */
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true ;
			
				} /* end else if-statement */
				else if( (magicSquares[i] == (15-(magicSquares[crossPieceIndexArray[1]] + magicSquares[crossPieceIndexArray[2]]))) && ((board.isFree(i)) == true ) && (nCrossPieceType == 3) && (squarePieceSet == false) )
				{
				
					/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
					from creating a win pattern */
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true ;
			   
				} /* end else if-statement */
				else if( (magicSquares[i] == (15-crossWinSum)) && (nCrossPieceType == 2) && ((board.isFree(i)) == true) && (squarePieceSet == false) )
				{
				
					/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
					from creating a win pattern */
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true ;
			
				} /* end else if-statement */

				i++;
			} /* end for-loop statement */
			
		} /* end if-statement */ 

		/* check if the computer have made more then 3 moves and if so then do something */
		if(nMoves > 3)
		{
			i = 0;
			/* For statement checks what kind of piece type that exists on the game board, when a cross piece is found
			it's index/location gets stored in the cross_piece_index_array[i], keep track on how many cross pieces we 
			have on the board */
			while(i < Board.N_SQUARES)
			{
				/* If we hava found a cross piece on the game board do something */
				if(board.hasPiece(Square.CROSS_PIECE,i) == true)
				{	
					
					crossPieceIndexArray[y] = i ;
					nCrossPieceType += 1 ;
					y++ ;

				} /* end if-statement */
				i++;
			} /* end for-statement */

			i = 0 ;
			/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
			while(i < Board.N_SQUARES)
			{

				if( (magicSquares[i] == (15-(magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[1]]))) && ((board.isFree(i)) == true) && (nCrossPieceType == 3) && (squarePieceSet == false))
				{
				   
					/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
					from creating a win pattern */
					noSetIndex = board.deleteRandomCirclePiece(Square.CIRCLE_PIECE) ;
					
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true ;

				} /* end if-statement */
				else if( (magicSquares[i] == (15-(magicSquares[crossPieceIndexArray[0]] + magicSquares[crossPieceIndexArray[2]])) && (board.isFree(i)) == true ) && (nCrossPieceType == 3) && (squarePieceSet == true))
				{
				   
					/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
					from creating a win pattern */
					noSetIndex = board.deleteRandomCirclePiece(Square.CIRCLE_PIECE) ;
			
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true ;

				} /* end else if-statement */
				else if( (magicSquares[i] == (15-(magicSquares[crossPieceIndexArray[1]] + magicSquares[crossPieceIndexArray[2]])) && (board.isFree(i)) == true ) && (nCrossPieceType == 3) && (squarePieceSet == false))
				{
		   
					/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
					from creating a win pattern */
					noSetIndex = board.deleteRandomCirclePiece(Square.CIRCLE_PIECE) ;
			
					board.setPiece(Square.CIRCLE_PIECE,i) ;
					squarePieceSet = true ;			   
			   
				} /* end else if-statement */
				i++;
			} /* end for-loop statement */

		} /* end if-statement */

	 	
		return squarePieceSet ;

	}

	/* the application context */
	Context context;
}