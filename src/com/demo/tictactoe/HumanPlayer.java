package com.demo.tictactoe;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

class HumanPlayer extends Player {
	
	static boolean madeMove = false;
	
	/* Human_Player constructor */
	public HumanPlayer(/* input arguments related to the type of piece stored on the square */
					   int pieceType, 
					   Context context)
	{
		super(pieceType);
		this.context = context;
	}
	
	
	/* make_move: make a move for the human player */
	public void makeMove(/* input arguments related to the amount of times the board_play_game runs */
						 int nMoves)
	{
		// Unimplemented function body
    }
	
	
	/* make_move: make a move for the human player */
	public void makeMove(/* input arguments related to the amount of times the board_play_game runs */
						 int nMoves, 
						 View from, View to)
	{  
		/* Check how many moves has been made so far in the game run */
		if(nMoves>3)
		  {
			
			if( from.getId() == R.id.square_1 )
				board.removePiece(0);
			else if(from.getId() == R.id.square_2 )
				board.removePiece(1);
			else if(from.getId() == R.id.square_3 )
				board.removePiece(2);
			else if(from.getId() == R.id.square_4 )
				board.removePiece(3);
			else if(from.getId() == R.id.square_5 )
				board.removePiece(4);
			else if(from.getId() == R.id.square_6 )
				board.removePiece(5);
			else if(from.getId() == R.id.square_7 )
				board.removePiece(6);
			else if(from.getId() == R.id.square_8 )
				board.removePiece(7);
			else if(from.getId() == R.id.square_9 )
				board.removePiece(8);			
			
			if( to.getId() == R.id.square_1 && board.isFree(0) )
			    board.setPiece(Square.CROSS_PIECE, 0);
			else if(to.getId() == R.id.square_2 && board.isFree(1))
			    board.setPiece(Square.CROSS_PIECE, 1);
			else if(to.getId() == R.id.square_3 && board.isFree(2))
			    board.setPiece(Square.CROSS_PIECE, 2);
			else if(to.getId() == R.id.square_4 && board.isFree(3))
			    board.setPiece(Square.CROSS_PIECE, 3);
			else if(to.getId() == R.id.square_5 && board.isFree(4))
			    board.setPiece(Square.CROSS_PIECE, 4);
			else if(to.getId() == R.id.square_6 && board.isFree(5))
			    board.setPiece(Square.CROSS_PIECE, 5);
			else if(to.getId() == R.id.square_7 && board.isFree(6))
			    board.setPiece(Square.CROSS_PIECE, 6);
			else if(to.getId() == R.id.square_8 && board.isFree(7))
			    board.setPiece(Square.CROSS_PIECE, 7);
			else if(to.getId() == R.id.square_9 && board.isFree(8))
			    board.setPiece(Square.CROSS_PIECE, 8);			

		  } /* end if statement */
		  	  
	   if(nMoves <= 3)
	   {   	
		    if( to.getId() == R.id.square_1 && board.isFree(0) )
		    	board.setPiece(Square.CROSS_PIECE, 0);
		    else if(to.getId() == R.id.square_2 && board.isFree(1))
		    	board.setPiece(Square.CROSS_PIECE, 1);
		    else if(to.getId() == R.id.square_3 && board.isFree(2))
		    	board.setPiece(Square.CROSS_PIECE, 2);
		    else if(to.getId() == R.id.square_4 && board.isFree(3))
		    	board.setPiece(Square.CROSS_PIECE, 3);
		    else if(to.getId() == R.id.square_5 && board.isFree(4))
		    	board.setPiece(Square.CROSS_PIECE, 4);
		    else if(to.getId() == R.id.square_6 && board.isFree(5))
		    	board.setPiece(Square.CROSS_PIECE, 5);
		    else if(to.getId() == R.id.square_7 && board.isFree(6))
		    	board.setPiece(Square.CROSS_PIECE, 6);
		    else if(to.getId() == R.id.square_8 && board.isFree(7))
		    	board.setPiece(Square.CROSS_PIECE, 7);
		    else if(to.getId() == R.id.square_9 && board.isFree(8))
		    	board.setPiece(Square.CROSS_PIECE, 8);
	   }   
		
	}
	
	/* the application context */
	Context context;
}
