package com.demo.tictactoe;

abstract class Player {
	
	//TODO: Use enum object for PieceType instead?
	/* Player constructor */
	public Player(int pieceType)
	{
		this.pieceType = pieceType;
		this.board = null;
	}
	
	/* use_board: makes the player use a board */
	public void useBoard(Board board)
	{

		this.board = board;

	}
	
	/* get_piece_type: returns a string with the piece
	type used by this player */
	String getPieceType()
	{
		if (Square.CROSS_PIECE == this.pieceType)
		{
			
			return "CROSSES";

		}
		else
		{

			return "CIRCLES";

		}
	}
	
	/* is_the_winner: returns true if this player is
	the winner of the game */
	public boolean isTheWinner()
	{
				
		return board.hasWinPattern(pieceType);

	}
	
	/* make_move: makes a move */
	abstract void makeMove(/* input arguments related to the amount of times the board_play_game runs */
						   int n_moves);
	
	private int pieceType;
	protected Board board;
}
