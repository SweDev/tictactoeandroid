package com.demo.tictactoe;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class TicTacToe extends Activity {
    
	public static final int BOARD_ROW_MAX = 3;	// Amount of rows in our board
	public static final int ROW_SQUARES_MAX = 3; // Amount of squares in a row
	
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
        UIWidgets = initWidgets();

    }
    
    /** Called when activity is started */
    @Override
    public void onStart()
    {
    	super.onStart();
    	
    	/* the game */
    	Game game = new Game(UIWidgets, this);
    	this.game = game;
    	board = this.game.getBoard();
    	human = this.game.getHumanPlayer();
    	computer = this.game.getComputerPlayer();
    	
    	AlertDialog.Builder dialog = new AlertDialog.Builder(TicTacToe.this);
		dialog.setTitle("TicTacToe Touch!");
		dialog.setMessage("Welcome to TicTacToe Touch!\nPlease, place three " +
				"cross pieces on the board. Once you have placed exactly three " +
				"cross pieces on the board you must move them each around by first pressing " +
				"at the cross piece you want to move and then pressing the square " +
				"you wish to place the cross piece onto. Remember that your objective is " +
				"to place three cross pieces in row, that is how you win the game.\n" +
				"Good luck!");
		dialog.show();
    	
    	/* play the game */
    	//game.play();
    }    
    
    /* Get widgets from resources and initialize them */
    public TextView[] initWidgets()
    {        
    	final TextView[] board = new TextView[9];
    	
        TextView textAbove = (TextView) findViewById(R.id.text_1);
        textAbove.setText("Tic-Tac-Toe Touch!");
        
        TableLayout boardRef = (TableLayout) findViewById(R.id.table);
        
        int n_rows = 0;		// simple counters
		int n_squares = 0;
		int squareIndex = 0;
        TableRow[] boardRowRefs = new TableRow[BOARD_ROW_MAX];
        
        /* Get all references to the rows of our board */
        while(n_rows < BOARD_ROW_MAX )
        {
        	boardRowRefs[n_rows] = (TableRow) boardRef.getChildAt(n_rows);
        	
        	/* Get all references to all the squares in a row */
        	while(squareIndex < ROW_SQUARES_MAX)
        	{
        		/* Store all the squares */
        		board[n_squares] = (TextView) boardRowRefs[n_rows].getVirtualChildAt(squareIndex);
        		n_squares++;	// increment total squares of board
        		squareIndex++;  // increment total squares of row
        	}
        	n_rows++;
        	squareIndex = 0;
        }
        
        n_squares = 0; // re-use counter
        /* Enable user interaction with squares */
        
        // Enable user interaction
        while( n_squares < ( BOARD_ROW_MAX * ROW_SQUARES_MAX ) )
        {	
        	board[n_squares].setOnClickListener(new View.OnClickListener() {
        		public void onClick(View v){
        			
        			Context context = TicTacToe.this;
        			String title = "Game Over";
        			String wonMessage = "";
        			String lostMessage = "";
        			String buttonString1 = "Play Again";
        			String buttonString2 = "Quit";
        			
        			AlertDialog.Builder dialog = new AlertDialog.Builder(TicTacToe.this);
					dialog.setTitle(title);
					dialog.setMessage(wonMessage);

					dialog.setPositiveButton(buttonString1,
											 new OnClickListener(){
												public void onClick(DialogInterface dialog,
																	int arg1){
													//TODO: Play again
													TicTacToe.board.removePiece(0);
													TicTacToe.board.removePiece(1);
													TicTacToe.board.removePiece(2);
													TicTacToe.board.removePiece(3);
													TicTacToe.board.removePiece(4);
													TicTacToe.board.removePiece(5);
													TicTacToe.board.removePiece(6);
													TicTacToe.board.removePiece(7);
													TicTacToe.board.removePiece(8);
												}
											 });
					
					 dialog.setNegativeButton(buttonString2,
							 				  new OnClickListener(){
											  	public void onClick(DialogInterface dialog,
											  						int arg1){	
											  		//TODO: Quit
											  		Intent intent = new Intent(Intent.ACTION_MAIN);
											  		intent.addCategory(Intent.CATEGORY_HOME); 
											  		startActivity(intent);
											  	}
							 				 });
					 /*
					 Toast.makeText(context, "1 free=" + TicTacToe.board.isFree(0), Toast.LENGTH_SHORT).show();
					 Toast.makeText(context, "2 free=" + TicTacToe.board.isFree(1), Toast.LENGTH_SHORT).show();
					 Toast.makeText(context, "3 free=" + TicTacToe.board.isFree(2), Toast.LENGTH_SHORT).show();
					 Toast.makeText(context, "4 free=" + TicTacToe.board.isFree(3), Toast.LENGTH_SHORT).show();
					 Toast.makeText(context, "5 free=" + TicTacToe.board.isFree(4), Toast.LENGTH_SHORT).show();
					 Toast.makeText(context, "6 free=" + TicTacToe.board.isFree(5), Toast.LENGTH_SHORT).show();
					 Toast.makeText(context, "7 free=" + TicTacToe.board.isFree(6), Toast.LENGTH_SHORT).show();
					 Toast.makeText(context, "8 free=" + TicTacToe.board.isFree(7), Toast.LENGTH_SHORT).show();
					 Toast.makeText(context, "9 free=" + TicTacToe.board.isFree(8), Toast.LENGTH_SHORT).show();
        			 */
        			Game.nMoves++;
        			
        			if(Game.nMoves <= 3 && from == null)
        			{
        				to = v;
        				//Toast.makeText(context, "to=" + TicTacToe.board.getIndex(to), Toast.LENGTH_SHORT).show();
        			}
        			else if(Game.nMoves > 3 && from != null)
        			{
        				to = v;
        				//Toast.makeText(context, "to=" + TicTacToe.board.getIndex(to), Toast.LENGTH_SHORT).show();
        			}
        			else if(Game.nMoves > 3 && to == null)
        			{
        				from = v;
        				//Toast.makeText(context, "from=" + TicTacToe.board.getIndex(from), Toast.LENGTH_SHORT).show();
        			}
        			
        			//Toast.makeText(TicTacToe.this, "Pressed!" , Toast.LENGTH_LONG).show();
        			if(Game.nMoves <= 3)
        			{	
        				if( TicTacToe.board.isFree(TicTacToe.board.getIndex(to)) )
        				{
        					Toast.makeText(context, "Nr moves: " + Game.nMoves, Toast.LENGTH_SHORT).show();
        					human.makeMove(Game.nMoves, null, to);
        					if(human.isTheWinner())
        					{
        						//Toast.makeText(context, "Congratulations, you won!" , Toast.LENGTH_LONG).show();
        						wonMessage = "Congratulations, you won after " + Game.nMoves +" moves!";
        						dialog.setMessage(wonMessage);
        						dialog.show();
        						Game.nMoves = 0;
        					}
        					computer.makeMove(Game.nMoves);
        					if(computer.isTheWinner())
        					{
        						//Toast.makeText(context, "Sorry, computer won!" , Toast.LENGTH_LONG).show();
        						lostMessage = "Sorry, computer won after " + Game.nMoves +" moves!";
        						dialog.setMessage(lostMessage);
        						dialog.show();
        						Game.nMoves = 0;
        					}
        				}
        				else
        				{	
        					Toast.makeText(context, "Please try again!", Toast.LENGTH_SHORT).show();
        					Game.nMoves--;
        				}
        				
        				// Reset
        				to = null;
        			}
        			else if(Game.nMoves > 3 && (from != null && to != null) )
        			{	
        				
        				if( (TicTacToe.board.isFree(TicTacToe.board.getIndex(to))) && !(TicTacToe.board.isFree(TicTacToe.board.getIndex(from))) && !(TicTacToe.board.hasPiece(Square.CIRCLE_PIECE,TicTacToe.board.getIndex(from))))
        				{
        					Toast.makeText(context, "Nr moves: " + Game.nMoves, Toast.LENGTH_SHORT).show();
        					//Toast.makeText(context, "Entered!", Toast.LENGTH_SHORT).show();
        					//Toast.makeText(context, "to is free=" + TicTacToe.board.isFree(TicTacToe.board.getIndex(to)), Toast.LENGTH_SHORT).show();
        					human.makeMove(Game.nMoves, from, to);
        					if(human.isTheWinner())
        					{
        						//Toast.makeText(context, "Congratulations, you won!" , Toast.LENGTH_LONG).show();
        						wonMessage = "Congratulations, you won after " + Game.nMoves +" moves!";
        						dialog.setMessage(wonMessage);
        						dialog.show();
        						Game.nMoves = 0;
        					}
        					else
        					{
        						computer.makeMove(Game.nMoves);
        						if(computer.isTheWinner())
        						{
        							//Toast.makeText(context, "Sorry, computer won!" , Toast.LENGTH_LONG).show();
        							lostMessage = "Sorry, computer won after " + Game.nMoves +" moves!";
        							dialog.setMessage(lostMessage);
        							dialog.show();
        							Game.nMoves = 0;
        						}
        					}
        				}
        				else
        				{
        					Toast.makeText(context, "Please try again!", Toast.LENGTH_SHORT).show();
        					Game.nMoves--;
        				}
        				
            			// Reset
            			from = null;
            			to = null;
        			}
        			else
        			{
        				Game.nMoves--;
        			}
        		}
        	});
        	n_squares++;	
        }
        
        return board;
    }
 
    private TextView[] UIWidgets;	// Reference to UI widgets
    private Game game;
    private static Board board;
    private HumanPlayer human;
    private ComputerPlayer computer;
    private View to;
    private View from;
}