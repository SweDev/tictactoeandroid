package com.demo.tictactoe;

import android.widget.TextView;

class Square{

	/* symbols for tic-tac-toe pieces */
	static final int NO_PIECE = -1;
	static final int CROSS_PIECE = 0;
	static final int CIRCLE_PIECE = 1;
	
	public Square(TextView square,int pieceType,boolean free)
	{		
		this.free = true;
		this.pieceType = pieceType; 
		this.square = square;
	}
	
	/* is_free: checks if the square is free */
	public boolean isFree()
	{
		return free ;
	}
	
	/* has_piece: returns true if a piece of type piece_type is placed on
	 *  the square, returns false otherwise */
	public boolean hasPiece(/* input arguments related to the type of piece stored on the square */
						     int pieceType)
	{
		if(pieceType == this.pieceType)
			return true ;
		else
			return false ;
	}
	
	/* square_remove_piece: removes a piece given by piece_type from a square */
	public void removePiece()
	{
		/* Here we delete a piece from a square */
		this.free = true ;
		this.pieceType = NO_PIECE ;
		this.square.setText("");
	}
	
	/* set_piece: places a piece of type piece_type on the square */
	public void setPiece(/* input arguments related to the type of 
						 piece stored on the square */
						 int pieceType)
	{
		this.free = false;
		this.pieceType = pieceType ;
		if(pieceType == Square.CIRCLE_PIECE)
			this.square.setText("O");
		else
			if(pieceType == Square.CROSS_PIECE)
				this.square.setText("X");
	}
	
	/* flag to indicate if a square is free or not */
	private boolean free;
	
	/* the type of piece stored on the square if the
	square is not free, in this case the admissible
	values are CROSS_PIECE and CIRCLE_PIECE,
	otherwise the value NO_PIECE is used */
	private int pieceType;
	private TextView square;
}
